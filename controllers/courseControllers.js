const Course = require("../models/Course");
const User = require("../models/User");

const auth = require("../auth");
const router = require("../routes/userRoutes");
const { updateOne } = require("../models/Course");
// Create a new course
/*
Steps:
1. Create a new course object
2. Save to the database
3. error handling

*/

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	//Create a new object
	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	//Saves the created object to our database
	return newCourse.save().then((course, error) => {
		//Course creation failed
		if(error) {
			return false;
		} else {
			//Course Creation successful
			return true;
		}
	})

}

//Get ALL courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    })
}


//Get all active courses
module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}

//Get specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams).then(result => {
        return result;
    })
}

//Update a course
/*
Steps:
1. Create variable which will contain the information retrieved from the request bidy
2. find and update course using course ID
*/

module.exports.updateCourse = (courseId, reqBody) => {
    //Spicify the properties of the doc to be updated
    let updateCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };
    //findByIdAndUpdate(id, updatesToBeApplied)
    return Course.findByIdAndUpdate(courseId, updateCourse).then((course, error) => {
        //course not updated
        if(error){
            return false;
        } else {
            return true;
        }
    })
}

//>>Activity 35
//archive 
module.exports.archiveCourse = (courseId, reqBody) => {
    let archive = {
        isActive: reqBody.isActive
    }
    return Course.findByIdAndUpdate(courseId, archive).then((course, error) => {
        if(error) {
            return false;
        } else {
            return true;
        }
    })
}


